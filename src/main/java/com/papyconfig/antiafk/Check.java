package com.papyconfig.antiafk;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.util.Vector;

import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.logging.Level;

public class Check extends TimerTask {

    private final JavaPlugin plugin;
    private final Integer chronoMessage;
    private final FileConfiguration config;
    private final Map<String, List<Double>> playersDir;

    public Check(JavaPlugin plugin, FileConfiguration config) {
        this.plugin = plugin;
        this.chronoMessage = config.getInt("chrono.message");
        this.config = config;
        this.playersDir = new HashMap<>();
    }

    @Override
    public void run() {
        plugin.getLogger().log(Level.INFO, "check afk farming...");

        // Get all connected players
        Collection<? extends Player> playersConnected = plugin.getServer().getOnlinePlayers();
        List<String> playersNames = new ArrayList<>();
        playersConnected.forEach((player -> {
            playersNames.add(player.getName());
        }));

        // First step is to clean playersDir
        playersDir.entrySet().removeIf(stringListEntry -> !playersNames.contains(stringListEntry.getKey()));

        // Then check player's facing direction
        for (Player player : playersConnected) {
            String playerName = player.getName();
            Vector playerDirection = player.getLocation().getDirection();
            double xDir = playerDirection.getX();
            double zDir = playerDirection.getZ();

            // Two cases possible
            // 1: Direction are the same => kick the player
            // 2: Direction are not the same, or new player => update playersDir
            if (playersDir.containsKey(playerName)) {

                List<Double> vector = playersDir.get(playerName);
                if (vector.get(0) == xDir && vector.get(1) == zDir) {
                    plugin.getLogger().log(Level.WARNING, playerName + " is suspicious !");
                    checkMessage(player);
                } else {
                    List<Double> newVector = new ArrayList<>(Arrays.asList(xDir, zDir));
                    playersDir.put(playerName, newVector);
                }
            } else {
                List<Double> newVector = new ArrayList<>(Arrays.asList(xDir, zDir));
                playersDir.put(playerName, newVector);
            }
        }
    }

    private void checkMessage(Player player) {
        // Add player to playersToKick, with a random String that the user must return
        String playerName = player.getName();
        player.sendMessage(Objects.requireNonNull(config.getString("kick-messages.warning")));

        CompletableFuture.runAsync(() -> {
            try {
                Thread.sleep(chronoMessage * 50);
            } catch (Exception e) {
                plugin.getLogger().log(Level.SEVERE, "Thread.sleep() failed");
                e.printStackTrace();
                plugin.getLogger().log(Level.SEVERE, "You have to manually reload the plugin");
            }

            // Check player's facing direction
            Vector playerDirection = player.getLocation().getDirection();
            double xDir = playerDirection.getX();
            double zDir = playerDirection.getZ();

            List<Double> vector = playersDir.get(playerName);
            if (vector.get(0) == xDir && vector.get(1) == zDir) {
                // Time to kick people
                plugin.getLogger().log(Level.SEVERE, playerName + " is afk farming, kick him !");
                int random = (int) Math.floor(Math.random() * config.getInt("kick-messages.nbMessages"));
                String message = config.getString("kick-messages." + random);
                Bukkit.getScheduler().runTask(plugin, () -> player.kickPlayer(message));
                playersDir.remove(playerName);
            } else {
                List<Double> newVector = new ArrayList<>(Arrays.asList(xDir, zDir));
                playersDir.put(playerName, newVector);
            }
        });
    }
}
